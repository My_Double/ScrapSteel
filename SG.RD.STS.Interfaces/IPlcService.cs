﻿namespace SG.RD.STS.Interfaces
{
    public interface IPlcService
    {
        bool Connect();
        void Read(byte[] readBytes);
        void Write(byte[] writeBytes);
    }
}