﻿/************************************************************************
 * 项目名称 :  HttpSelfHost.Controllers  
 * 项目描述 :     
 * 类 名 称 :  SteelRecorderController
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 16:41:33
 * 更新时间 :  2020/3/11 16:41:33
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System;
using System.Collections.Generic;
using System.Web.Http;
using SG.RD.STS.Models;
using SG.RD.STS.Repository.Entity;

namespace HttpSelfHost.Controllers
{
    public class SteelRecorderController:ApiController
    {
        /// <summary>
        /// 获取全部物料信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<SteelRecorder> SteelRecorderList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取物料分页信息
        /// </summary>
        /// <param name="info">分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public PageTable<SteelRecorder> SteelRecorderPage(PageInfo info)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 获取一条物料信息
        /// </summary>
        /// <param name="steelRecorder"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult GetSteelRecorder(SteelRecorder steelRecorder)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 修改物料信息
        /// </summary>
        /// <param name="steelRecorder"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult UpdateSteelRecorder(SteelRecorder steelRecorder)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 新增物料信息
        /// </summary>
        /// <param name="steelRecorder"></param>
        /// <returns></returns>
        [HttpPut]
        public IHttpActionResult AddSteelRecorder(SteelRecorder steelRecorder)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 删除物料信息
        /// </summary>
        /// <param name="steelRecorder"></param>
        /// <returns></returns>
        public IHttpActionResult DeleteSteelRecorder(SteelRecorder steelRecorder)
        {
            throw new NotImplementedException();
        }
    }
}
