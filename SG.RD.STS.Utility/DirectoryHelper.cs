﻿using System;
using System.IO;

namespace SG.RD.STS.Utility
{
    public class DirectoryHelper
    {
        /// <summary>
        /// 获取服务器基础目录
        /// </summary>
        /// <returns></returns>
        public static string GetBasePath()
        {
            string basePath = AppDomain.CurrentDomain.BaseDirectory;

            if (basePath.IndexOf(@"\bin\Debug") > -1)
                basePath = basePath.Replace(@"\bin\Debug", "");

            return basePath;
        }

        /// <summary>
        /// 获取设备配置文件基础目录
        /// </summary>
        /// <returns></returns>
        public static string GetFacCfgPath()
        {
            return Path.Combine(GetBasePath(), @"Configrations\FacilityConfig.xml");
        }
    }
}