﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository.Entity  
 * 项目描述 :     
 * 类 名 称 :  Schedule
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 10:59:59
 * 更新时间 :  2020/3/11 10:59:59
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SG.RD.STS.Repository.Enums;

namespace SG.RD.STS.Repository.Entity
{
    /// <summary>
    /// 计划
    /// </summary>
    [Table("SCHEDULE")]
    public class Schedule:BaseEntity
    {
        /// <summary>
        /// 批次号
        /// </summary>
        [Column("BATCHNO")]
        public int BatchNo { get; set; }
        /// <summary>
        /// 钢种编号
        /// </summary>
        [Column("STEELID")]
        public int SteelId { get; set; }
        /// <summary>
        /// 备料时间
        /// </summary>
        [Column("PREPARETIME")]
        public DateTime PrepareTime { get; set; }
        /// <summary>
        /// 模式号
        /// </summary>
        [Column("PATTENNO")]
        [MaxLength(45)]
        public string PattenNo { get; set; }
        /// <summary>
        /// 三级计划号
        /// </summary>
        [Column("PLANNOL3")]
        [MaxLength(45)]
        public string  PlanNoL3 { get; set; }
        /// <summary>
        /// 计划状态
        /// </summary>
        [Column("STATUS")]
        public ScheduleStat Status { get; set; }
        /// <summary>
        /// 总重量
        /// </summary>
        [Column("SUMWEIGHT")]
        public int SumWeight { get; set; }
        /// <summary>
        /// 钢斗号
        /// </summary>
        [Column("BUCKETNO")]
        public int BucketNo { get; set; }

    }
}
