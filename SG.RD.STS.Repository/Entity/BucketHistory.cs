﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository.Entity  
 * 项目描述 :     
 * 类 名 称 :  BucketHistory
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 21:03:23
 * 更新时间 :  2020/3/11 21:03:23
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SG.RD.STS.Repository.Enums;

namespace SG.RD.STS.Repository.Entity
{
    /// <summary>
    /// 钢斗历史信息
    /// </summary>
    [Table("BUCKETHISTORY")]
    public class BucketHistory:BaseEntity
    {
        /// <summary>
        /// 斗号
        /// </summary>
        [MaxLength(45)]
        [Column("BUCKETNAME")]
        public string BucketName { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column("STATUS")]
        public BucketStat Status { get; set; }
        /// <summary>
        /// 炉次
        /// </summary>
        [Column("STOVENO")]
        public int StoveNo { get; set; }
        /// <summary>
        /// 净重
        /// </summary>
        [Column("NETWEIGHT")]
        public int NetWeight { get; set; }
        /// <summary>
        /// 使用次数
        /// </summary>
        [Column("USECOUNT")]
        public int UseCount { get; set; }
        /// <summary>
        /// 三级ID
        /// </summary>
        [Column("IDLV3")]
        [MaxLength(45)]
        public string IdLv3 { get; set; }
        /// <summary>
        /// 计划序号
        /// </summary>
        [Column("PLANNO")]
        [MaxLength(45)]
        public string PlanNo { get; set; }
    }
}
