﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SG.RD.STS.Repository.Entity
{
    [Table("TEST")]
    public class Test
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [MaxLength(45)]
        [Column("UNAME")]
        public string UName { get; set; }
        [MaxLength(45)]
        [Column("UPSW")]
        public string UPsw { get; set; }
        [Column("PERM")]
        public int? Perm { get; set; }
    }
}
