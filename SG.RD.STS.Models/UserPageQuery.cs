﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Domain.Model  
 * 项目描述 :     
 * 类 名 称 :  UserPageQuery
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/2/25 19:51:34
 * 更新时间 :  2020/2/25 19:51:34
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

namespace SG.RD.STS.Models
{
    /// <summary>
    /// 用户查询
    /// </summary>
    public class UserPageQuery
    {
        /// <summary>
        /// 页面
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// 页面大小
        /// </summary>
        public int PageSize { get; set; }


        /// <summary>
        /// 用户权限
        /// </summary>
        public int? UserPerm { get; set; }

    }
}
