﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using SG.RD.STS.Interfaces;
using SG.RD.STS.Models;
using SG.RD.STS.Utility;

namespace HttpSelfHost.Forms
{
    public partial class SitConfig : Form
    {
        private Host _host;
        private const string home = "api/values/test";

        public SitConfig()
        {
            InitializeComponent();
        }

        private void SitConfig_Load(object sender, EventArgs e)
        {
            //设置窗体居中
            int x = (SystemInformation.WorkingArea.Width - this.Size.Width) / 2;
            int y = (SystemInformation.WorkingArea.Height - this.Size.Height) / 2;
            StartPosition = FormStartPosition.Manual; //窗体的位置由Location属性决定
            Location = (Point)new Size(x, y);         //窗体的起始位置为(x,y)

            //设置初始化的数据
            _host = SystemConfig.GetValue<Host>("Host");
            this.txbAddr.Text = _host.Ip;
            this.txbPort.Text = _host.Port.ToString();
            this.txbDir.Text = AppDomain.CurrentDomain.BaseDirectory;
        }

        private void SitConfig_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)//最小化事件
            {
                //this.Hide();//最小化时窗体隐藏
                this.ShowInTaskbar = false;
                this.notifyIcon.Visible = true;
            }
        }

        private void SitConfig_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result;
            result = MessageBox.Show("确定退出吗？", "退出", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == DialogResult.OK)
            {

                Application.ExitThread();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:    //左击
                    //判断是否已经最小化于托盘 
                    if (WindowState == FormWindowState.Minimized)
                    {
                        //还原窗体显示 
                        WindowState = FormWindowState.Normal;
                        //激活窗体并给予它焦点 
                        this.Activate();
                        //任务栏区显示图标 
                        this.ShowInTaskbar = true;
                        //托盘区图标隐藏 
                        this.notifyIcon.Visible = false;
                    }
                    break;
                case MouseButtons.Right:
                    break;
            }
        }

        private void cmiConfig_Click(object sender, EventArgs e)
        {

        }

        private void cmiHome_Click(object sender, EventArgs e)
        {
            OpenBrowser();
        }

        private void cmiMgr_Click(object sender, EventArgs e)
        {

        }

        private void cmiDir_Click(object sender, EventArgs e)
        {
            OpenDir();
        }

        private void cmiRestart_Click(object sender, EventArgs e)
        {

        }

        private void cmiQuit_Click(object sender, EventArgs e)
        {
            Application.ExitThread();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //更改地址后需要重新启动owin host。

            //IConfig config;
            //var info = config.ReadConfig();
            //info.Host.Ip = txbAddr.Text.TrimEnd();

            //config.SetConfig(info);

            Program.RestartWebTask();
        }

        private void btnPort_Click(object sender, EventArgs e)
        {

        }

        private void btnDir_Click(object sender, EventArgs e)
        {
            OpenDir();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {

        }

        private void btnRestart_Click(object sender, EventArgs e)
        {

        }

        private void btnManager_Click(object sender, EventArgs e)
        {

        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            OpenBrowser();
        }

        private void ExitMainThread(object sender, EventArgs e)
        { }


        private void OpenDir()
        {
            if (!string.IsNullOrEmpty(txbDir.Text))
                System.Diagnostics.Process.Start(txbDir.Text);
        }

        private void OpenBrowser()
        {
            var targetUrl = string.Format("http://{0}:{1}/{2}", _host.Ip, _host.Port, home);
            BrowserHelper.OpenBrowserUrlFireFox(targetUrl);
        }
    }
}
