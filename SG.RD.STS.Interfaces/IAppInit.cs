﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Interfaces  
 * 项目描述 :     
 * 类 名 称 :  IAppInit接口
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  接口
 * 作    者 :  张臣
 * 创建时间 :  2020/5/11 22:38:22
 * 更新时间 :  2020/5/11 22:38:22
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SG.RD.STS.Interfaces
{
    public interface IAppInit
    {
        void Start();
    }
}
