﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository.Entity  
 * 项目描述 :  物流   
 * 类 名 称 :  Logistics
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 10:34:15
 * 更新时间 :  2020/3/11 10:34:15
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SG.RD.STS.Repository.Entity
{
    /// <summary>
    /// 废钢物流信息
    /// </summary>
    [Table("LOGISTICS")]
    public class Logistics:BaseEntity
    {
        /// <summary>
        /// 创建人
        /// </summary>
        [Column("CREATOR")]
        public int Creator { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATTIME")]
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 版本
        /// </summary>
        [Column("VERSION")]
        public int Version { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        [Column("MODIFIER")]
        public int Modifier { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFYTIME")]
        public DateTime ModifyTime { get; set; }
        /// <summary>
        /// 称重单号
        /// </summary>
        [Column("WEIGHTNO")]
        [MaxLength(45)]
        public string WeightNo { get; set; }
        /// <summary>
        /// 车牌号
        /// </summary>
        [Column("CARNO")]
        [MaxLength(45)]
        public string CarNo { get; set; }
        /// <summary>
        /// 物料ID
        /// </summary>
        [Column("STEELID")]
        public int SteelId { get; set; }
        /// <summary>
        /// 发货库存地编码
        /// </summary>
        [Column("SOURCESTORAGENO")]
        [MaxLength(45)]
        public string SourceStorageNo { get; set; }
        /// <summary>
        /// 发货库存地名称
        /// </summary>
        [Column("SOURCESTORAGENAME")]
        [MaxLength(45)]
        public string SourceStorageName { get; set; }
        /// <summary>
        /// 净重
        /// </summary>
        [Column("NETWEIGHT")]
        public int NetWeight { get; set; }
        /// <summary>
        /// 毛重
        /// </summary>
        [Column("ROUGHWEIGHT")]
        public int RoughWeight { get; set; }
        /// <summary>
        /// 确认转存时间
        /// </summary>
        [Column("CONFIRMTIME")]
        public DateTime ConfirmTime { get; set; }
    }
}
