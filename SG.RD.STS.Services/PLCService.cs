﻿using System;
using System.Threading;
using S7Comm;
using SG.RD.STS.Interfaces;
using SG.RD.STS.Models.PLC;
using SG.RD.STS.Utility;

namespace SG.RD.STS.Services
{
    /// <summary>
    /// PLC服务类
    /// </summary>
    public class PlcService : IPlcService
    {
        private static S7Client client = null;
        private static ManualResetEvent TimeOutObj = new ManualResetEvent(false);
        private static bool IsConnected = false;
        public static PlcService Connection
        {
            get { return SingleInstance<PlcService>.GetInstance(); }
        }

        private PlcService()
        {

        }

        public bool Connect()
        {
            client = new S7Client();
            var conn = PLCConn.Instance;
            TimeOutObj.Reset();
            try
            {
                client.ConnectTo(conn.IP, conn.Rack, conn.Slot);
            }
            catch (Exception e)
            {
                Log.Error("PLC connection Erro!", e);
            }

            if (TimeOutObj.WaitOne(1000, false))
            {
                if (IsConnected)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public void Read(byte[] readBytes)
        {
            var info = PLCConn.Instance;
            var conn = PLCBuffer.Instance;
            try
            {
                if (client == null)
                {
                    Connect();
                }
                else if (!client.Connected)
                {
                    //if (!IsConnect())
                    //{
                    //    ReConnect();
                    //}
                    return;
                }

                int bytesRec = client.DBRead(conn.ReadNo, conn.ReadAllStart, conn.ReadAllSize, readBytes);

                if (bytesRec == 0)
                {
                    Log.Warning("Warnig! 0 bytes received!");
                }

            }
            catch (Exception e)
            {
                Log.Error("PLC Read Error", e);
            }

        }

        public void Write(byte[] writeBytes)
        {
            var info = PLCBuffer.Instance;
            //if (CheckState())
            //{
            try
            {
                client.DBWrite(info.WriteNo, info.WriteStart, info.WriteSize, writeBytes);
            }
            catch (Exception e)
            {
                Log.Error("write db erro!", e);
            }
            //}

        }

        /// <summary>
        /// 重连
        /// </summary>
        /// <returns></returns>
        private bool ReConnect()
        {
            client.Disconnect();
            IsConnected = false;

            return Connect();
        }

        /// <summary>
        /// 当连接为false时，进一步确定当前的连接状态
        /// </summary>
        private bool IsConnect()
        {
            var connectState = true;
            var conn = PLCConn.Instance;
            var info = PLCBuffer.Instance;
            try
            {
                byte[] tmpBytes = new Byte[1];
                client.DBWrite(info.WriteNo, 0, 1, tmpBytes);
                connectState = true;
            }
            catch (Exception e)
            {
                connectState = false;
                Log.Error(e.StackTrace, e);
            }

            return connectState;
        }

        /// <summary>
        /// 检测client的状态
        /// </summary>
        /// <returns></returns>
        private bool CheckState()
        {
            try
            {
                if (client == null)
                {
                    return Connect();
                }
                else if (IsConnected)
                {
                    return true;
                }
                else
                    return false;
               
            }
            catch (Exception e)
            {
                Log.Error("Check state erro!", e);
                return false;
            }
        }
    }



}