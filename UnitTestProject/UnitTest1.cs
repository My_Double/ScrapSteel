﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SG.RD.STS.Repository.Entity;
using System.Collections.Generic;
using System.Linq;
using SG.RD.STS.Repository.Enums;
using SG.RD.STS.Services;
using SG.RD.STS.Utility;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestEnumDisplayName()
        {
            var Name=SuckerStat.Off.GetDisplayName();

            Assert.AreEqual("去磁",Name);
        }

        [TestMethod]
        public void TestPageTable()
        {
            List<Logistics> logisticsList=new List<Logistics>();
            var car1=new Logistics(){CarNo="1"};
            var car2=new Logistics(){CarNo="2"};
            logisticsList.Add(car1);
            logisticsList.Add(car2);
            logisticsList.Add(new Logistics(){CarNo="3"});
            logisticsList.Add(new Logistics(){CarNo="4"});
           
            var test=logisticsList.ToPageTable(1,2).Data.ToList();
            var excepted=new List<Logistics>();
            excepted.Add(car1);
            excepted.Add(car2);

            var a=test.All(excepted.Contains);
            var b= test.Count()==excepted.Count;
           
            Assert.AreEqual(true,a&&b);
        }

        [TestMethod]
        public void AutoFac()
        {

        }
    }
}
