﻿/************************************************************************
 * 项目名称 :  HttpSelfHost.Controllers  
 * 项目描述 :     
 * 类 名 称 :  BucketController
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 16:41:33
 * 更新时间 :  2020/3/11 16:41:33
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System;
using System.Collections.Generic;
using System.Web.Http;
using SG.RD.STS.Models;
using SG.RD.STS.Repository.Entity;

namespace HttpSelfHost.Controllers
{
    public class BucketController:ApiController
    {

        /// <summary>
        /// 获取全部钢斗信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<Bucket> BucketList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取钢斗分页信息
        /// </summary>
        /// <param name="info">分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public PageTable<Bucket> BucketPage(PageInfo info)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 获取一条钢斗信息
        /// </summary>
        /// <param name="bucket"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult GetBucket(Bucket bucket)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 修改钢斗信息
        /// </summary>
        /// <param name="bucket"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult UpdateBucket(Bucket bucket)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 新增钢斗信息
        /// </summary>
        /// <param name="bucket"></param>
        /// <returns></returns>
        [HttpPut]
        public IHttpActionResult AddBucket(Bucket bucket)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 删除钢斗信息
        /// </summary>
        /// <param name="bucket"></param>
        /// <returns></returns>
        [HttpDelete]
        public IHttpActionResult DeleteBucket(Bucket bucket)
        {
            throw new NotImplementedException();
        }
    }
}
