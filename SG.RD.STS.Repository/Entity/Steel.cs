﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository.Entity  
 * 项目描述 :     
 * 类 名 称 :  Steel
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/10 11:15:12
 * 更新时间 :  2020/3/10 11:15:12
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SG.RD.STS.Repository.Entity
{
    /// <summary>
    /// 钢种
    /// </summary>
    [Table("STEEL")]
    public class Steel:BaseEntity
    {
        /// <summary>
        /// 钢种名称
        /// </summary>
        [Column("NAME")]
        [MaxLength(45)]
        public string Name { get; set; }
        /// <summary>
        /// 配比模式号
        /// </summary>
        [Column("PATTENNO")]
        public int PattenNo { get; set; }
        /// <summary>
        /// 半配比模式号
        /// </summary>
        [Column("HALFPATTENNO")]
        public int HalfPattenNo { get; set; }

    }
}
