﻿/************************************************************************
 * 项目名称 :  HttpSelfHost  
 * 项目描述 :     
 * 类 名 称 :  Startup
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/5/12 8:33:42
 * 更新时间 :  2020/5/12 8:33:42
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System;
using System.IO;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.Owin;
using Owin;
using SG.RD.STS.Utility;

namespace HttpSelfHost
{
    public class Startup
    {
        private static string _siteDir = ".";

        /// This code configures Web API contained in the class Startup,
        /// which is additionally specified as the type parameter in WebApplication.Start
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for Self-Host
            HttpConfiguration config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { action = RouteParameter.Optional, id = RouteParameter.Optional }
            );


            //json 序列化设置
            config.Formatters
                .JsonFormatter.SerializerSettings = new
                Newtonsoft.Json.JsonSerializerSettings()
                {
                    //NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
                    DateFormatString = "yyyy-MM-dd HH:mm:ss" //设置时间日期格式化
                };

            config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            config.Formatters.JsonFormatter.MediaTypeMappings.Add(new QueryStringMapping("datatype", "json", "application/json"));

            var builder = new ContainerBuilder();
            //指定已扫描程序集中的类型注册为提供所有其实现的接口。
            var services = Assembly.Load("SG.RD.STS.Services");
            builder.RegisterAssemblyTypes(services).AsImplementedInterfaces();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
         

            SwaggerConfig.Register(config);

            appBuilder.UseAutofacMiddleware(container);
            appBuilder.UseAutofacWebApi(config);
            appBuilder.UseWebApi(config);

            //静态文件
            appBuilder.Use((context, fun) =>
            {
                return StaticWebFileHandel(context, fun);
            });
        }



        #region 客户端请求静态文件处理
        /// <summary>
        /// 客户端请求静态文件处理
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public Task StaticWebFileHandel(IOwinContext context, Func<Task> next)
        {
            //获取物理文件路径
            var path = GetFilePath(context.Request.Path.Value);
            //验证路径是否存在
            if (!File.Exists(path) && !path.EndsWith("html"))
            {
                path += ".html";
            }
            if (File.Exists(path))
            {
                return SetResponse(context, path);
            }
            //不存在返回下一个请求
            return next();
        }
        #endregion

        #region 路由初始化
        /// <summary>
        /// 路由初始化
        /// </summary>
        public HttpConfiguration InitWebApiConfig()
        {
            HttpConfiguration config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { id = RouteParameter.Optional }
            );
            // 配置 http 服务的路由
            var cors = new EnableCorsAttribute("*", "*", "*");//跨域允许设置
            config.EnableCors(cors);

            config.Formatters
               .XmlFormatter.SupportedMediaTypes.Clear();
            //默认返回 json
            config.Formatters
                .JsonFormatter.MediaTypeMappings.Add(
                new QueryStringMapping("datatype", "json", "application/json"));
            //返回格式选择
            config.Formatters
                .XmlFormatter.MediaTypeMappings.Add(
                new QueryStringMapping("datatype", "xml", "application/xml"));

            //json 序列化设置
            //config.Formatters
            //    .JsonFormatter.SerializerSettings = new
            //    Newtonsoft.Json.JsonSerializerSettings()
            //    {
            //        //NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
            //        DateFormatString = "yyyy-MM-dd HH:mm:ss" //设置时间日期格式化
            //    };
            return config;
        }
        #endregion

        #region 获取文件路径

        public static string GetFilePath(string relPath)
        {
            if (relPath.IndexOf("index") > -1)
            {
                String temp = relPath;
            }
            var basePath = DirectoryHelper.GetBasePath();

            var filePath = relPath.TrimStart('/').Replace('/', '\\');
            if (_siteDir == ".")
            {
                return Path.Combine(basePath, filePath);
            }
            else
            {
                return Path.Combine(
                  AppDomain.CurrentDomain.BaseDirectory
                  , _siteDir == "." ? "" : _siteDir
                  , relPath.Replace('/', '\\')).TrimStart('\\');
            }
        } 
        #endregion

        #region 设置响应类型

        public Task SetResponse(IOwinContext context, string path)
        {
            /*
                .txt text/plain
                RTF文本 .rtf application/rtf
                PDF文档 .pdf application/pdf
                Microsoft Word文件 .word application/msword
                PNG图像 .png image/png
                GIF图形 .gif image/gif
                JPEG图形 .jpeg,.jpg image/jpeg
                au声音文件 .au audio/basic
                MIDI音乐文件 mid,.midi audio/midi,audio/x-midi
                RealAudio音乐文件 .ra, .ram audio/x-pn-realaudio
                MPEG文件 .mpg,.mpeg video/mpeg
                AVI文件 .avi video/x-msvideo
                GZIP文件 .gz application/x-gzip
                TAR文件 .tar application/x-tar
                任意的二进制数据 application/octet-stream
             */
            var perfix = Path.GetExtension(path);
            switch (perfix)
            {
                case ".html":
                    context.Response.ContentType = "text/html; charset=utf-8";
                    break;
                case ".txt":
                    context.Response.ContentType = "text/plain";
                    break;
                case ".js":
                    context.Response.ContentType = "application/x-javascript";
                    break;
                case ".css":
                    context.Response.ContentType = "text/css";
                    break;
                default:
                    switch (perfix)
                    {
                        case ".jpeg":
                        case ".jpg":
                            context.Response.ContentType = "image/jpeg";
                            break;
                        case ".gif":
                            context.Response.ContentType = "image/gif";
                            break;
                        case ".png":
                            context.Response.ContentType = "image/png";
                            break;
                        case ".svg":
                            context.Response.ContentType = "image/svg+xml";
                            break;
                        case ".woff":
                            context.Response.ContentType = "application/font-woff";
                            break;
                        case ".woff2":
                            context.Response.ContentType = "application/font-woff2";
                            break;
                        case ".ttf":
                            context.Response.ContentType = "application/octet-stream";
                            break;
                    }

                    return context.Response.WriteAsync(File.ReadAllBytes(path));
            }
            return context.Response.WriteAsync(File.ReadAllText(path));
        } 
        #endregion

    }

}
