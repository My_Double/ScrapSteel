﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Domain.Model  
 * 项目描述 :     
 * 类 名 称 :  CrownBridge
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/12 10:31:57
 * 更新时间 :  2020/3/12 10:31:57
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

namespace SG.RD.STS.Models
{
    /// <summary>
    /// 地磅
    /// </summary>
    public class CrownBridge
    {
        /// <summary>
        /// 地磅号
        /// </summary>
        public int BridgeNo { get; set; }
        /// <summary>
        /// 地磅名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 净重
        /// </summary>
        public int NetWeight { get; set; }
        /// <summary>
        /// 盈亏重量
        /// </summary>
        public int YKWeight { get; set; }
        /// <summary>
        /// 计划重量
        /// </summary>
        public int PlanWeight { get; set; }
        /// <summary>
        /// 皮重
        /// </summary>
        public int RoughWeight { get; set; }
        /// <summary>
        /// 积累净重
        /// </summary>
        public int SumWeight { get; set; }
    }
}
