﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository.Entity  
 * 项目描述 :     
 * 类 名 称 :  Patten
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 20:55:52
 * 更新时间 :  2020/3/11 20:55:52
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SG.RD.STS.Repository.Entity
{
    /// <summary>
    /// 配比模式
    /// </summary>
    [Table("PATTEN")]
    public class Patten:BaseEntity
    {
        /// <summary>
        /// 模式号
        /// </summary>
        [Column("PATTENNO")]
        [MaxLength(45)]
        public string PattenNo { get; set; }
        /// <summary>
        /// 重量
        /// </summary>
        [Column("WEIGHT")]
        public int Weight { get; set; }
        /// <summary>
        /// 配比类型
        /// </summary>
        [Column("KIND")]
        public int Kind { get; set; }
    }
}
