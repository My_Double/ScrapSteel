﻿namespace SG.RD.STS.Repository
{
    using SG.RD.STS.Repository.Entity;
    using SG.RD.STS.Repository.Migrations;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class MyDBContext : DbContext
    {
        //您的上下文已配置为从您的应用程序的配置文件(App.config 或 Web.config)
        //使用“MyDBContext”连接字符串。默认情况下，此连接字符串针对您的 LocalDb 实例上的
        //“SG.RD.STS.Repository.MyDBContext”数据库。
        // 
        //如果您想要针对其他数据库和/或数据库提供程序，请在应用程序配置文件中修改“MyDBContext”
        //连接字符串。
        public MyDBContext()
            : base("name = OracleDbContext")//MyContextConn
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<MyDBContext, Configuration>());
            Database.SetInitializer(new DropCreateDatabaseAlways<MyDBContext>());
        }

        //为您要在模型中包含的每种实体类型都添加 DbSet。有关配置和使用 Code First  模型
        //的详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=390109。

        public virtual DbSet<BaseEntity> BaseEntities { get; set; }
        public virtual DbSet<Test> Tests { get; set; }
        public virtual DbSet<Bucket> Buckets { get; set; }
        public virtual DbSet<BucketHistory> BucketHistories { get; set; }
        public virtual DbSet<Logistics> Logistics { get; set; }
        public virtual DbSet<Material> Materials { get; set; }
        public virtual DbSet<Patten> Pattens { get; set; }
        public virtual DbSet<Schedule> Schedules { get; set; }
        public virtual DbSet<Steel> Steels { get; set; }
        public virtual DbSet<SteelPool> SteelPools { get; set; }
        public virtual DbSet<SteelRecorder> SteelRecorders { get; set; }
        public virtual DbSet<SteelStorage> SteelStorages { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.HasDefaultSchema("FDDTEST")中的字符串参数为 Oracle数据库用户名（大写）
            modelBuilder.HasDefaultSchema("FDDTEST");
            //默认在数据库中生成的表名为实体模型类名的复数形式,modelBuilder.Conventions.Remove语句中的 OnModelCreating 方法会阻止表名变为复数形式
            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}