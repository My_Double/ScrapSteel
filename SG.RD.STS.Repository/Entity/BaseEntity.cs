﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository.Entity  
 * 项目描述 :     
 * 类 名 称 :  BaseEntity
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/2/26 9:44:18
 * 更新时间 :  2020/2/26 9:44:18
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SG.RD.STS.Repository.Entity
{
    /// <summary>
    /// 实体基类
    /// </summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// ID
        /// </summary>
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        /// <summary>
        /// 可用性
        /// </summary>
        /// <remarks>
        /// true 可用；fasle不可用，但数据库中存在
        /// </remarks>
        [Column("ENABLE")]
        public bool Enable { get; set; }
    }
}
