﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Utility  
 * 项目描述 :     
 * 类 名 称 :  EnumerableEx
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/5/11 17:13:23
 * 更新时间 :  2020/5/11 17:13:23
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System.Collections.Generic;
using System.Linq;
using SG.RD.STS.Models;

namespace SG.RD.STS.Services
{
    /// <summary>
    /// 集合类型的扩展为分页方法
    /// </summary>
    public static class EnumerableEx
    {
        /// <summary>
        /// 分页获取
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">全部数据</param>
        /// <param name="index">当前页索引</param>
        /// <param name="size">每页记录数量</param>
        /// <returns></returns>
        public static PageTable<T> ToPageTable<T>(this IQueryable<T> data, int index, int size)
        {
            if (index < 1)
            {
                index=1;
            }

            var current=data.Skip((index-1)*size).Take(size);

            var paged=new PageTable<T>(current,index,size,data.Count());

            return paged;
        }

        public static PageTable<T> ToPageTable<T>(this IQueryable<T> data,PageInfo info)
        {
            return data.ToPageTable(info.Index,info.Size);  
        }
        public static PageTable<T> ToPageTable<T>(this IEnumerable<T> data,PageInfo info)
        {
            return data.AsQueryable().ToPageTable(info.Index,info.Size);  
        }
        /// <summary>
        /// 分页获取
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">全部数据</param>
        /// <param name="index">当前页索引</param>
        /// <param name="size">每页记录数量</param>
        public static PageTable<T> ToPageTable<T>(this IEnumerable<T> data, int index, int size)
        {
            return data.AsQueryable().ToPageTable(index,size);
        }
    }
}
