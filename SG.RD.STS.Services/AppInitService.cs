﻿
/************************************************************************
 * 项目名称 :  SG.RD.STS.Services  
 * 项目描述 :     
 * 类 名 称 :  AppInitService
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/5/11 22:39:44
 * 更新时间 :  2020/5/11 22:39:44
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG.RD.STS.Interfaces;

namespace SG.RD.STS.Services
{
    public class AppInitService : IAppInit
    {
        private readonly IConfig _config;

        public AppInitService(IConfig config)
        {
            this._config = config;
        }

        public void Start()
        {
            /*
         * 整体流程：
         * 载入配置文件，开启通讯连接（PLC连接），开启Http监听，向前端推送数据的websocket
         */
           var configs= _config.ReadConfig();
        }   
    }
}
