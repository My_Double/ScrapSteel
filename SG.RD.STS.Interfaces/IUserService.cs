﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Domain.Interfaces  
 * 项目描述 :     
 * 类 名 称 :  IUserService
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/2/25 20:25:48
 * 更新时间 :  2020/2/25 20:25:48
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using SG.RD.STS.Models;
using SG.RD.STS.Repository.Entity;

namespace SG.RD.STS.Interfaces
{
    /// <summary>
    /// 权限管理
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// 获取用户
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        User UserAccess(string userName, string password);

        /// <summary>
        /// 获取用户分页信息
        /// </summary>
        /// <param name="pageQuery">页面信息</param>
        /// <returns>用户分页信息</returns>
        UserTable GetUserList(UserPageQuery pageQuery);

        /// <summary>
        /// 更新用户信息
        /// </summary>
        /// <param name="user">用户信息</param>
        /// <returns>是否成功</returns>
        bool UpdateUser(User user);

        /// <summary>
        /// 增加用户信息
        /// </summary>
        /// <param name="user">用户信息</param>
        /// <returns>是否成功</returns>
        bool AddUser(User user);

        /// <summary>
        /// 删除用户信息
        /// </summary>
        /// <param name="user">用户信息</param>
        /// <returns>是否成功</returns>
        bool DeleteUser(User user);
    }
}
