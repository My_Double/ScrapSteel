﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository.Entity  
 * 项目描述 :     
 * 类 名 称 :  Bucket
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 10:56:58
 * 更新时间 :  2020/3/11 10:56:58
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SG.RD.STS.Repository.Entity
{
    /// <summary>
    /// 钢斗
    /// </summary>
    [Table("BUCKET")]
    public class Bucket:BaseEntity
    {
        [MaxLength(45)]
        [Column("NAME")]
        public string  Name { get; set; }
    }
}
