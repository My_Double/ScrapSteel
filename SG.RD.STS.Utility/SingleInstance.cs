﻿using System;

namespace SG.RD.STS.Utility
{
    /// <summary>
    ///  单例泛型
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SingleInstance<T> where T : class
    {
        private static readonly object objlock = new object();
        private static T instance;

        private SingleInstance(){}

        public static T GetInstance()
        {
            if (instance == null)
            {
                lock (objlock)
                {
                    if (instance == null)
                    {
                        instance = (T)Activator.CreateInstance(typeof(T), true);
                    }
                }
            }

            return instance;
        }
    }
}