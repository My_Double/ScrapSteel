﻿using System;
using SG.RD.STS.Interfaces;
using SG.RD.STS.Models;
using SG.RD.STS.Repository;
using SG.RD.STS.Repository.Entity;
using SG.RD.STS.Repository.Enums;
using SG.RD.STS.Utility;

namespace SG.RD.STS.Services
{
    public class UserService : IUserService
    {

        IEncrypt ie = ServiceLocator.Instance.GetService<IEncrypt>();

        public User UserAccess(string userName, string password)
        {
            User tmpUser = new User();

            var pswd = ie.md5(password.Trim(), 16);

            User users;
            using (var db = DbFactory.Create())
            {
                users = db.Get<User>(x => x.UserName == userName & x.UserKey == pswd);
            }

            if (users != null)
            {
                //数据库包含用户权限
                tmpUser = users;
            }
            else
            {
                //创建临时用户权限
                tmpUser.UserName = userName;
                tmpUser.UserPerm = EPerm.Tmp;
            }

            return tmpUser;
        }

        public UserTable GetUserList(UserPageQuery pageQuery)
        {
            UserTable userTable = new UserTable();

            using (var iRepository = DbFactory.Create())
            {
                int total = 0;

                try
                {

                    if (pageQuery.UserPerm == 1)
                    {
                        userTable.UserList = iRepository.Find<User, int>(u => (int)u.UserPerm != 6, u => u.Id, pageQuery.PageSize, pageQuery.Page, out total);
                    }
                    else
                    {
                        userTable.UserList = iRepository.Find<User, int>(u => (int)u.UserPerm != 1 && (int)u.UserPerm != 6, u => u.Id, pageQuery.PageSize, pageQuery.Page, out total);
                    }

                    userTable.TotalItems = total;

                    return userTable;
                }
                catch (Exception ex)
                {
                    Log.Error("用户数据读取失败", ex);

                    return null;
                }
            }
        }


        public bool UpdateUser(User user)
        {
            using (var iRepository = DbFactory.Create())
            {
                try
                {
                    //判断用户是否存在
                    User tmpUser = iRepository.Get<User>(x => x.UserId == user.UserId);

                    if (tmpUser == null)
                    {
                        return false;
                    }

                    tmpUser.UserName = user.UserName;

                    //md5加密
                    tmpUser.UserKey = ie.md5(user.UserKey, 16);

                    tmpUser.UserIp = user.UserIp;

                    tmpUser.UserPerm = user.UserPerm;

                    iRepository.Update<User>(tmpUser);

                    iRepository.SaveChanges();

                    return true;
                }
                catch (Exception ex)
                {
                    Log.Error("更新用户数据错误", ex);

                    return false;
                }
            }
        }

        public bool AddUser(User user)
        {
            using (var iRepository = DbFactory.Create())
            {
                try
                {
                    //判断重名用户
                    User tmpUser = iRepository.Get<User>(x => x.UserName == user.UserName);

                    if (tmpUser != null)
                    {
                        return false;
                    }

                    tmpUser = new User();

                    tmpUser.UserName = user.UserName;

                    //md5加密
                    tmpUser.UserKey = ie.md5(user.UserKey, 16);

                    tmpUser.UserIp = user.UserIp;

                    tmpUser.UserPerm = user.UserPerm;

                    iRepository.Insert<User>(tmpUser);

                    iRepository.SaveChanges();

                    return true;
                }
                catch (Exception ex)
                {
                    Log.Error("新增用户数据错误", ex);

                    return false;
                }
            }
        }

        public bool DeleteUser(User user)
        {
            using (var iRepository = DbFactory.Create())
            {
                try
                {

                    //判断用户是否存在
                    User tmpUser = iRepository.Get<User>(x => x.UserId == user.UserId);

                    if (tmpUser == null)
                    {
                        return false;
                    }

                    iRepository.Delete<User>(tmpUser);

                    iRepository.SaveChanges();

                    return true;
                }
                catch (Exception ex)
                {
                    Log.Error("删除用户数据错误", ex);

                    return false;
                }
            }
        }
    }
}
