﻿
/************************************************************************
 * 项目名称 :  HttpSelfHost.Controllers  
 * 项目描述 :     
 * 类 名 称 :  UserController
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 15:54:26
 * 更新时间 :  2020/3/11 15:54:26
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System;
using System.Web.Http;
using SG.RD.STS.Models;
using SG.RD.STS.Repository.Entity;

namespace HttpSelfHost.Controllers
{
    public class UserController: ApiController
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="user">用户登陆信息</param>
        /// <returns>用户权限信息</returns>
        [HttpPost]
        public User Login(User user)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="pageQuery">基础分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public UserTable GetUserList(UserPageQuery pageQuery)
        {
           throw new NotImplementedException();
        }

        /// <summary>
        /// 更新用户信息
        /// </summary>
        /// <param name="user">用户信息</param>
        /// <returns></returns>
        [HttpPost]
        public bool UpdateUser(User user)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 新增用户信息
        /// </summary>
        /// <param name="user">用户信息</param>
        /// <returns></returns>
        [HttpPost]
        public bool AddUser(User user)
        {
           throw new NotImplementedException();
        }

        /// <summary>
        /// 删除用户信息
        /// </summary>
        /// <param name="user">用户信息</param>
        /// <returns></returns>
        [HttpPost]
        public bool DeleteUser(User user)
        {
            throw new NotImplementedException();
        }
    }
}
