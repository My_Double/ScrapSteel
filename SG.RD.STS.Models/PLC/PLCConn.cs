﻿using SG.RD.STS.Utility;

namespace SG.RD.STS.Models.PLC
{
    /// <summary>
    /// PLC连接类
    /// </summary>
    public class PLCConn
    {
        /// <summary>
        /// IP地址
        /// </summary>
        public string IP { get; set; }
        /// <summary>
        /// 机架号
        /// </summary>
        public int Rack { get; set; }
        /// <summary>
        /// 插槽号
        /// </summary>
        public int Slot { get; set; }

        private PLCConn()
        {
        }

        public static PLCConn Instance
        {
            get { return SystemConfig.GetValue<PLCConn>("PLC"); }
        }
    }
}