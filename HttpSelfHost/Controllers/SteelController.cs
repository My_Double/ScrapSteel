﻿
/************************************************************************
 * 项目名称 :  HttpSelfHost.Controllers  
 * 项目描述 :     
 * 类 名 称 :  SteelController
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/10 11:23:38
 * 更新时间 :  2020/3/10 11:23:38
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System;
using System.Collections.Generic;
using System.Web.Http;
using SG.RD.STS.Interfaces;
using SG.RD.STS.Models;
using SG.RD.STS.Repository.Entity;

namespace HttpSelfHost.Controllers
{
    /// <summary>
    /// 钢种基础信息
    /// </summary>
    public class SteelController:ApiController
    {
        

        /// <summary>
        /// 获取全部钢种信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<Steel> SteelList() 
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 获取单个钢种
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]        
        public Steel GetSteel(int id)
        {            
            throw new NotImplementedException();
        }

        /// <summary>
        /// 通过废钢池位置获取钢种
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        [HttpPost]
        public Steel GetSteel(Location location)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 获取钢种分页信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        [HttpPost]
        public PageTable<Steel> SteelPage(PageInfo info)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 修改钢种信息
        /// </summary>
        /// <param name="steel"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult UpdateSteel(Steel steel)
        {
            throw new NotImplementedException();            
        }
        /// <summary>
        /// 新增钢种
        /// </summary>
        /// <param name="steel"></param>
        /// <returns></returns>
        [HttpPut]
        public IHttpActionResult AddSteel(Steel steel)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 删除一个钢种
        /// </summary>
        /// <param name="steel"></param>
        /// <returns></returns>
        [HttpDelete]
        public IHttpActionResult Delete(Steel steel)
        {
            throw new NotImplementedException();
        }
    }
}
