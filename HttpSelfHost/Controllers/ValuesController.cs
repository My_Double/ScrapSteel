﻿
using System.Web.Http;
using SG.RD.STS.Interfaces;

namespace HttpSelfHost.Controllers
{
    public class ValuesController : ApiController
    {
        private readonly IConfig _config;
        private readonly IAppInit _app;
        public ValuesController(IConfig config,IAppInit app)
        {
            this._config = config;
            this._app = app;
        }
        [HttpGet]
        public string Test()
        {
            _app.Start();
            return _config.ReadConfig().ToString();
        }
    }
}