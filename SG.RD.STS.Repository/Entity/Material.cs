﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository.Entity  
 * 项目描述 :     
 * 类 名 称 :  Material
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 14:59:59
 * 更新时间 :  2020/3/11 14:59:59
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SG.RD.STS.Repository.Entity
{
    /// <summary>
    /// 物料信息
    /// </summary>
    [Table("MATERIAL")]
    public class Material : BaseEntity
    {
        /// <summary>
        /// 物料名称
        /// </summary>
        [Column("NAME")]
        [MaxLength(45)]
        public string Name { get; set; }
        /// <summary>
        /// 二级ID
        /// </summary>
        [Column("IDLV2")]
        public int IdLv2 { get; set; }
        /// <summary>
        /// 二级描述
        /// </summary>
        [Column("DESCRIBLELV2")]
        [MaxLength(45)]
        public string DescribleLv2 { get; set; }
        /// <summary>
        /// 三级ID
        /// </summary>
        [Column("IDLV3")]
        public int IdLv3 { get; set; }
        /// <summary>
        /// 三级描述
        /// </summary>
        [Column("DESCRIBLELV3")]
        [MaxLength(45)]
        public string DescribleLv3 { get; set; }
    }
}
