﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using HttpSelfHost.Forms;
using SG.RD.STS.Models;
using SG.RD.STS.Utility;

namespace HttpSelfHost
{
    static class Program
    {
        private static System.Threading.Mutex mutex;
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.ThreadException += Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            mutex = new System.Threading.Mutex(true, "ScrapTreatment");
            if (mutex.WaitOne(0, false))
            {
                AppStart();
                Application.Run(new SitConfig());

            }
            else
            {
                MessageBox.Show("程序已经在运行！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Exit();
            }
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Exception ex = e.Exception;
            MessageBox.Show(string.Format("捕获到未处理异常：{0}\r\n异常信息：{1}\r\n异常堆栈：{2}", ex.GetType(), ex.Message, ex.StackTrace));
            Log.Error(string.Format("捕获到未处理异常：{0}\r\n异常信息：{1}\r\n异常堆栈：{2}", ex.GetType(), ex.Message, ex.StackTrace), ex);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;
            MessageBox.Show(string.Format("捕获到未处理异常：{0}\r\n异常信息：{1}\r\n异常堆栈：{2}\r\nCLR即将退出：{3}", ex.GetType(), ex.Message, ex.StackTrace, e.IsTerminating));
            Log.Error(string.Format("捕获到未处理异常：{0}\r\n异常信息：{1}\r\n异常堆栈：{2}", ex.GetType(), ex.Message, ex.StackTrace), ex);
        }


        private static IDisposable owin;
        static void AppStart()
        {
            Task.Factory.StartNew(OwinInit, TaskCreationOptions.LongRunning);
        }

        public static void RestartWebTask()
        {
            owin.Dispose();
            Task.Factory.StartNew(OwinInit);
        }

        static void OwinInit()
        {
            var host = SystemConfig.GetValue<Host>("Host");
            //http://localhost:10281/api/values
            var baseAddress = string.Format("http://{0}:{1}/", host.Ip, host.Port);

            owin = Microsoft.Owin.Hosting.WebApp.Start<Startup>(url: baseAddress);
        }

    }
}
