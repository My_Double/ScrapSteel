﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SG.RD.STS.Interfaces; /************************************************************************
 * 项目名称 :  SG.RD.STS.Domain.Concretes  
 * 项目描述 :     
 * 类 名 称 :  ObserverBase
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/2/25 20:20:08
 * 更新时间 :  2020/2/25 20:20:08
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

namespace SG.RD.STS.Services
{
    #region 基础观察者
    /// <summary>
    /// 基础观察者
    /// </summary>
    public class ObserverBase : IObservable
    {
        private List<IObserver> observerList = new List<IObserver>();
        /// <summary>
        /// 添加观察者
        /// </summary>
        /// <param name="observer"></param>
        public void AddObserver(IObserver observer)
        {
            this.observerList.Add(observer);
        }

        /// <summary>
        /// 删除观察者
        /// </summary>
        /// <param name="observer"></param>
        public void DeleteObserver(IObserver observer)
        {
            this.observerList.Remove(observer);
        }

        /// <summary>
        /// 通知观察者
        /// </summary>
        /// <param name="context"></param>
        public async void NotifyObserver(object context)
        {
            await Task.Run(() =>
            {
                foreach (IObserver observer in observerList)
                {
                    observer.Update(context);
                }
            });
        }
     
    }
    #endregion
}
