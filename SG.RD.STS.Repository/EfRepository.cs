﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository  
 * 项目描述 :     
 * 类 名 称 :  EfRepository
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/2/26 9:40:47
 * 更新时间 :  2020/2/26 9:40:47
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SG.RD.STS.Repository
{
    public class EfRepository : IRepository
    {
        
        #region 变量

        private MyDBContext context;

        #endregion

        #region 构造函数

        public EfRepository(MyDBContext dbcontext)
        {
            context = dbcontext;
        }

        #endregion
        
        #region IRepository

        public IQueryable<T> All<T>() where T : class
        {
            return context.Set<T>().AsNoTracking();
        }

        public void Update<T>(T entity) where T : class
        {
            var entry = context.Entry(entity);
            if (entry.State == EntityState.Detached)
            {
                context.Set<T>().Attach(entity);
            }
            entry.State = EntityState.Modified;
        }

        public T Insert<T>(T entity) where T : class
        {
            return context.Set<T>().Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            var entry = context.Entry(entity);
            if (entry.State == EntityState.Detached)
            {
                context.Set<T>().Attach(entity);
            }
            entry.State = EntityState.Deleted;
            context.Set<T>().Remove(entity);
        }

        public void Delete<T>(Expression<Func<T, bool>> conditions) where T : class
        {
            var list = Find<T>(conditions);
            foreach (var item in list)
            {
                Delete<T>(item);
            }

        }

        public T Get<T>(Expression<Func<T, bool>> conditions) where T : class
        {
            return All<T>().FirstOrDefault(conditions);
        }

        public List<T> Find<T>(Expression<Func<T, bool>> conditions = null) where T : class
        {
            if (conditions != null)
            {
                return All<T>().Where(conditions).ToList();
            }
            else
            {
                return All<T>().ToList();
            }

        }

        public List<T> Find<T, S>(Expression<Func<T, bool>> conditions, Expression<Func<T, S>> orderBy, int pageSize, int pageIndex, out int totalCount) where T : class
        {
            var queryList = conditions == null ?
                All<T>() :
                All<T>().Where(conditions);

            totalCount = queryList.Count();

            return queryList.OrderByDescending(orderBy).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
        }

        public List<T> SqlQuery<T>(string sql)
        {
            return context.Database.SqlQuery<T>(sql).ToList();
        }

        public int ExecuteSqlCommand(string sql)
        {
            return context.Database.ExecuteSqlCommand(sql);
        }

        public int SaveChanges()
        {
            return context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public long GetNextSequenceValue(string sequenceName)
        {
            var rawQuery = context.Database.SqlQuery<long>(string.Format("SELECT NEXT VALUE FOR {0}", sequenceName)).ToList();
            long nextVal = rawQuery.Single();

            return nextVal;
        }

        #endregion
    }
}
