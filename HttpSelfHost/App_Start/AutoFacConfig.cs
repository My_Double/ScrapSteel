﻿using System.Reflection;
using Autofac;
/************************************************************************
 * 项目名称 :  HttpSelfHost.App_Start  
 * 项目描述 :     
 * 类 名 称 :  AutoFacConfig
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/5/11 22:50:40
 * 更新时间 :  2020/5/11 22:50:40
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

namespace HttpSelfHost
{
    public class AutoFacConfig
    {
        /// <summary>
        /// 使用AutoFac实现依赖注入
        /// </summary>
        private void autoFac()
        {
            var builder = new ContainerBuilder();
            SetupResolveRules(builder);  //注入

            //builder.RegisterControllers(Assembly.GetExecutingAssembly());  //注入所有Controller
            var container = builder.Build();
            //DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        private void SetupResolveRules(ContainerBuilder builder)
        {
            //UI项目只用引用service和repository的接口，不用引用实现的dll。
            //如需加载实现的程序集，将dll拷贝到bin目录下即可，不用引用dll
            var IServices = Assembly.Load("SG.RD.STS.Interfaces");
            var Services = Assembly.Load("SG.RD.STS.Services");


            //根据名称约定（服务层的接口和实现均以Service结尾），实现服务接口和服务实现的依赖
            builder.RegisterAssemblyTypes(IServices, Services)
                //.Where(t => t.Name.EndsWith("Service"))
              .AsImplementedInterfaces();

            ////根据名称约定（数据访问层的接口和实现均以Repository结尾），实现数据访问接口和数据访问实现的依赖
            //builder.RegisterAssemblyTypes(IRepository, EfRepository)
            //    //.Where(t => t.Name.EndsWith("Repository"))
            //  .AsImplementedInterfaces();
        }

    }
}
