﻿using Sg.RD.Rc_ArgusIII.Domain.Interfaces;
using Sg.RD.Rc_ArgusIII.Domain.Model;
using Sg.RD.Rc_ArgusIII.Repository.Entity;
namespace Sg.RD.Rc_ArgusIII.Domain.Concretes
{
    public class DataService : ICurrentData
    {
        private static Monitor _currentData;
        private IWebSocketService iwss;

        private IMaintainService ims;
        private IProcService ipro;

        public Monitor CurrentData
        {
            get
            {
                if (_currentData == null)
                {
                    _currentData = new Monitor();
                }
                return _currentData;

            }
            set
            {
                _currentData = value;
            }
        }


        public DataService()
        {
            uivm = new UIVM();
            iwss = new WebSocketService();//ServiceLocator.Instance.GetService<IWebSocketService>();
            ims = new MaintainService();
            ipro = new ProcService();

        }

        public void BegainMonitor(string ip)
        {
            iwss.ServerStart(ip);
        }


        private static UIVM uivm;
    }
}