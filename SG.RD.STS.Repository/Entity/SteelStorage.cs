﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository.Entity  
 * 项目描述 :     
 * 类 名 称 :  SteelStorage
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 11:37:40
 * 更新时间 :  2020/3/11 11:37:40
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System.ComponentModel.DataAnnotations.Schema;

namespace SG.RD.STS.Repository.Entity
{
    /// <summary>
    /// 废钢库存
    /// </summary>
    /// <remarks>
    /// 库存表计算方式：
    /// 
    /// 入库：入库时，向SteelRecorder添加记录，然后计算SteelStorage
    /// 
    /// 出库检查：只有出库数量小于等于现有数量时，才可以出库
    /// 如果某种钢消耗完，那么需要将该钢的记录从现有库存种移除
    /// 
    /// 现有数量计算：SteelStorage为现有物料库存
    /// </remarks>
    [Table("STEELSTORAGE")]
    public class SteelStorage:BaseEntity
    {
        /// <summary>
        /// 物料ID
        /// </summary>
        [Column("STEELID")]
        public int SteelId { get; set; }
        /// <summary>
        /// 操作重量
        /// </summary>
        [Column("WEIGHT")]
        public int Weight { get; set; }
    }
}
