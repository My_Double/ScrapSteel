﻿/************************************************************************
 * 项目名称 :  HttpSelfHost.Controllers  
 * 项目描述 :     
 * 类 名 称 :  LogisticsController
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 16:41:33
 * 更新时间 :  2020/3/11 16:41:33
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System;
using System.Collections.Generic;
using System.Web.Http;
using SG.RD.STS.Models;
using SG.RD.STS.Repository.Entity;

namespace HttpSelfHost.Controllers
{
    public class LogisticsController:ApiController
    {

        /// <summary>
        /// 获取全部物流信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<Logistics> LogisticsList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取物流分页信息
        /// </summary>
        /// <param name="info">分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public PageTable<Logistics> LogisticsPage(PageInfo info)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 获取一条物流信息
        /// </summary>
        /// <param name="logistics"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult GetLogistcs(Logistics logistics)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 修改物流信息
        /// </summary>
        /// <param name="logistics"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult UpdateLogistcs(Logistics logistics)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 新增物流信息
        /// </summary>
        /// <param name="logistics"></param>
        /// <returns></returns>
        [HttpPut]
        public IHttpActionResult AddLogistcs(Logistics logistics)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 删除物流信息
        /// </summary>
        /// <param name="logistics"></param>
        /// <returns></returns>
        [HttpDelete]
        public IHttpActionResult DeleteLogistcs(Logistics logistics)
        {
            throw new NotImplementedException();
        }
    }
}
