﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository.Entity  
 * 项目描述 :     
 * 类 名 称 :  SteelRecorder
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 12:04:45
 * 更新时间 :  2020/3/11 12:04:45
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SG.RD.STS.Repository.Entity
{
    /// <summary>
    /// 物料记录表
    /// </summary>
    /// <remarks>
    /// 用于记录进出库的物料
    /// </remarks>
    [Table("STEELRECORDER")]
    public class SteelRecorder:BaseEntity
    {
        /// <summary>
        /// 物料ID
        /// </summary>
        [Column("STEELID")]
        public int SteelId { get; set; }
        /// <summary>
        /// 操作重量
        /// </summary>
        [Column("WEIGHT")]
        public int Weight { get; set; }
        /// <summary>
        /// 操作时间
        /// </summary>
        [Column("OPERATETIME")]
        public DateTime OperateTime { get; set; }
        /// <summary>
        /// 进出库标志
        /// </summary>
        /// <remarks>
        /// 值为true：入库；值为false：出库
        /// </remarks>
        [Column("INOROUT")]
        public bool InOrOut { get; set; }
        /// <summary>
        /// 位置
        /// </summary>
        [Column("LOCATION")]
        public int Location { get; set; }
    }
}
