﻿
/************************************************************************
 * 项目名称 :  HttpSelfHost.Controllers  
 * 项目描述 :     
 * 类 名 称 :  TransferController
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/12 11:22:06
 * 更新时间 :  2020/3/12 11:22:06
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System;
using System.Web.Http;
using SG.RD.STS.Repository.Entity;

namespace HttpSelfHost.Controllers
{
    /// <summary>
    /// 通讯类接口
    /// </summary>
    public class TransferController:ApiController
    {
        /// <summary>
        /// 发送计划给转炉
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult SendToStove(Schedule schedule)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 取消发送计划
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult CancleToStove(Schedule schedule)
        {
            throw new NotImplementedException();
        }
    }
}
