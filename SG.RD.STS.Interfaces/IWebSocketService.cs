﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Domain.Interfaces  
 * 项目描述 :     
 * 类 名 称 :  IWebSocketService
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/2/25 20:28:04
 * 更新时间 :  2020/2/25 20:28:04
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

namespace SG.RD.STS.Interfaces
{
    /// <summary>
    /// websocket接口
    /// </summary>
    public interface IWebSocketService
    {
        void Send(object msg);

        void ServerStart(string ip);
    }
}
