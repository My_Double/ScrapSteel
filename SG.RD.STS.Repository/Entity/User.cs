﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository.Entity  
 * 项目描述 :     
 * 类 名 称 :  User
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/2/26 9:43:23
 * 更新时间 :  2020/2/26 9:43:23
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System;
using System.ComponentModel.DataAnnotations.Schema;
using SG.RD.STS.Repository.Enums;

namespace SG.RD.STS.Repository.Entity
{
    /// <summary>
    /// 用户
    /// </summary>
    [Table("USER")]
    public class User:BaseEntity
    {
        [Column("USERID")]
        public int UserId { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        [Column("USERNAME")]
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Column("USERKEY")]
        public string UserKey { get; set; }

        /// <summary>
        /// 用户权限
        /// </summary>
        [Column("USERPERM")]
        public EPerm UserPerm { get; set; }

        /// <summary>
        /// 用户IP地址
        /// </summary>
        [Column("USERIP")]
        public string UserIp { get; set; }

        /// <summary>
        /// 最后访问时间
        /// </summary>
        [Column("VISITTIME")]
        public DateTime VisitTime { get; set; }
    }
}
