﻿using System;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "./Conf/Log.config", Watch = true)]

namespace SG.RD.STS.Utility
{
    /// <summary>
    /// 日志记录类
    /// </summary>
    public static class Log
    {
        /// <summary>
        /// 错误记录
        /// </summary>
        /// <param name="msg">消息</param>
        public static void Error(string msg)
        {
            log4net.ILog log = log4net.LogManager.GetLogger(msg);
            log.Error(msg);
        }
        /// <summary>
        /// 消息记录
        /// </summary>
        /// <param name="msg">消息</param>
        public static void Info(string msg)
        {
            log4net.ILog log = log4net.LogManager.GetLogger(msg);
            log.Info(msg);
        }
        /// <summary>
        /// 警告记录
        /// </summary>
        /// <param name="msg">消息</param>
        public static void Warning(string msg)
        {
            log4net.ILog log = log4net.LogManager.GetLogger(msg);
            log.Warn(msg);
        }
        /// <summary>
        /// 错误记录
        /// </summary>
        /// <param name="msg">消息</param>
        /// <param name="ex">异常</param>
        public static void Error(string msg, Exception ex)
        {
            log4net.ILog log = log4net.LogManager.GetLogger(msg);
            log.Error("Error", ex);
        }
        /// <summary>
        /// 信息记录
        /// </summary>
        /// <param name="msg">消息</param>
        /// <param name="ex">异常</param>
        public static void Info(string msg, Exception ex)
        {
            log4net.ILog log = log4net.LogManager.GetLogger(msg);
            log.Info("Info", ex);
        }
        /// <summary>
        /// 警告记录
        /// </summary>
        /// <param name="msg">消息</param>
        /// <param name="ex">异常</param>
        public static void Warning(string msg, Exception ex)
        {
            log4net.ILog log = log4net.LogManager.GetLogger(msg);
            log.Warn("Warning", ex);
        }
    }
}