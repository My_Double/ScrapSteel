﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Models  
 * 项目描述 :     
 * 类 名 称 :  Host
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/5/11 16:27:17
 * 更新时间 :  2020/5/11 16:27:17
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SG.RD.STS.Models
{
    public class Host
    {
        public string Ip { get; set; }

        public int Port { get; set; }
    }
}
