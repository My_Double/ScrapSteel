﻿using SG.RD.STS.Interfaces;
using SG.RD.STS.Models;
/************************************************************************
 * 项目名称 :  SG.RD.STS.Services  
 * 项目描述 :     
 * 类 名 称 :  ConfigService
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/5/12 20:17:09
 * 更新时间 :  2020/5/12 20:17:09
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG.RD.STS.Utility;

namespace SG.RD.STS.Services
{
    public class ConfigService:IConfig
    {
        public Config ReadConfig()
        {
            return SystemConfig.GetAllValue<Config>();
        }

        public void SetConfig(Config config)
        {
            SystemConfig.Write(config);
        }
    }
}
