﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Windows.Forms;
using SG.RD.STS.Utility;

namespace HttpSelfHost.Forms
{
    public partial class MainForm : Form
    {
    
        public MainForm()
        {
            InitializeComponent();
            
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            CombBoxFullFill();
            //设置鼠标放在托盘图标上面的文字
            this.notifyIcon.Text = "废钢系统";
        }

        private void MainForm_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)//最小化事件
            {
                //this.Hide();//最小化时窗体隐藏
                this.ShowInTaskbar = false;
                this.notifyIcon.Visible = true;
            }
        }
        public void UpdateMsg(string str)
        {
            if (txtMsg.InvokeRequired)
            {
                // 当一个控件的InvokeRequired属性值为真时，说明有一个创建它以外的线程想访问它
                Action<string> actionDelegate = (x) => { txtMsg.Text += x.ToString() + "\r\n"; };
                // 或者
                // Action<string> actionDelegate = delegate(string txt) { this.txtMsg.Text = txt; };
                txtMsg.Invoke(actionDelegate, str);
            }
            else
            {
                txtMsg.Text += str + "\r\n";
            }
        }

        private void MainForm_DoubleClick(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Show();
                this.WindowState = FormWindowState.Normal; //还原窗体
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result;
            result = MessageBox.Show("确定退出吗？", "退出", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == DialogResult.OK)
            {
                
                Application.ExitThread();
            }
            else
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// 获取本机IP地址
        /// </summary>
        /// <returns>本机IP地址</returns>
        public IPAddress[] GetLocalIP()
        {
            try
            {
                string HostName = Dns.GetHostName(); //得到主机名
                IPHostEntry IpEntry = Dns.GetHostEntry(HostName);

                return IpEntry.AddressList;
            }
            catch (Exception ex)
            {
                var self = IPAddress.Parse("127.0.0.1");
                IPAddress[] add = new IPAddress[] { self };
                Log.Error(ex.Message, ex);
                return add;
            }
        }
        private string GetUrl()
        {
            var ip = this.cmbIP.SelectedItem.ToString();
            var port = int.Parse(ConfigurationManager.AppSettings.Get("Port"));
            return string.Format("http://{0}:{1}/pages/index", ip,port);
        }

        private void CombBoxFullFill()
        {
            var ips = GetLocalIP();
            if (ips.Length >= 1)
            {
                foreach (var ip in ips)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                        this.cmbIP.Items.Add(ip.ToString());
                }

                this.cmbIP.SelectedIndex = 0;
            }
        }

        private void notifyIcon_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:    //左击
                    //判断是否已经最小化于托盘 
                    if (WindowState == FormWindowState.Minimized)
                    {
                        //还原窗体显示 
                        WindowState = FormWindowState.Normal;
                        //激活窗体并给予它焦点 
                        this.Activate();
                        //任务栏区显示图标 
                        this.ShowInTaskbar = true;
                        //托盘区图标隐藏 
                        this.notifyIcon.Visible = false;
                    }
                    break;
                case MouseButtons.Right:
                    //MessageBox.Show("右键");
                    break;
            }
        }

        private void ShowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否确认退出程序？", "退出", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                // 关闭所有的线程
                this.Dispose();
                this.Close();
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.linkLabel1.Links[this.linkLabel1.Links.IndexOf(e.Link)].Visited = true;
            string targetUrl = e.Link.LinkData as string;
            if (string.IsNullOrEmpty(targetUrl))
                MessageBox.Show("没有链接地址！");
            else
                BrowserHelper.OpenBrowserUrlFireFox(targetUrl);
        }

        private void btnRewrite_Click(object sender, EventArgs e)
        {
           //HttpServer.WritePLC();

            Log.Info("写到PLC");
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            var ip = this.cmbIP.SelectedItem.ToString();
            if (ip != string.Empty)
            {
                var address = GetUrl();
                this.linkLabel1.Links.Add(0, address.Length, address);
               // ControlAgent.InintMain(this);
                Task.Run(() =>
                {
                    //HttpServer.Init(ip);
                });
            }

            this.btnOK.Enabled = false;
        }


    }
}
