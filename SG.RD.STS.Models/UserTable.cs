﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Domain.Model  
 * 项目描述 :     
 * 类 名 称 :  UserTable
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/2/25 19:50:39
 * 更新时间 :  2020/2/25 19:50:39
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System.Collections.Generic;
using System.Linq;
using SG.RD.STS.Repository.Entity;

namespace SG.RD.STS.Models
{
    public class UserTable
    {
        /// <summary>
        /// 总数据条数
        /// </summary>
        public int TotalItems { get; set; }

        /// <summary>
        /// 用户集合
        /// </summary>
        public List<User> UserList { get; set; }

        public UserTable() 
        {
            this.UserList = new List<User>();
        }
    }

    /// <summary>
    /// 分页信息
    /// </summary>
    public class PageInfo
    {
        public int Index { get; set; }
        public int Size { get; set; }
    }
    /// <summary>
    /// 分页接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IPageTable<T>
    {
        /// <summary>
        /// 页
        /// </summary>
        int Index { get;set;}
        /// <summary>
        /// 页大小
        /// </summary>
        int Size { get;set;}
        /// <summary>
        /// 总数
        /// </summary>
        int Total { get;set;}
        /// <summary>
        /// 数据
        /// </summary>
        IEnumerable<T> Data { get;set;}
    }
    public class PageTable<T>:IPageTable<T>
    {
        public int Index { get; set; }
        public int Size { get; set; }
        public int Total { get; set; }
        public IEnumerable<T> Data { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="current">当前页数据</param>
        /// <param name="index">当前页索引</param>
        /// <param name="size">每页记录数量</param>
        /// <param name="total">总记录数量</param>
        public PageTable(IQueryable<T> current,int index,int size,int total)
        {
            this.Index=index;
            this.Size=size;
            this.Total=total;
            this.Data=current;
        }
    }

   
}
