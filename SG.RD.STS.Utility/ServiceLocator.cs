﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace SG.RD.STS.Utility
{
    /// <summary>
    /// Unity依赖注入
    /// </summary>
    public class ServiceLocator : IServiceProvider
    {
        private readonly IUnityContainer _container;

        private static readonly ServiceLocator instance = new ServiceLocator();

        private ServiceLocator()
        {
            UnityConfigurationSection section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            _container = new UnityContainer();
            section.Configure(_container);
        }

        public static ServiceLocator Instance
        {
            get { return instance; }
        }
        public object GetService(Type serviceType)
        {
            return _container.Resolve(serviceType);
        }

        public T GetService<T>()
        {
            return _container.Resolve<T>();
        }
        /// <summary>
        /// 按名称获取注册的映射关系
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <returns></returns>
        public T GetService<T>(string type)
        {
            return _container.Resolve<T>(type);
        }
        /// <summary>
        /// 获取容器中所有的注册的已命名对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerable<T> GetAll<T>()
        {
            return _container.ResolveAll<T>();
        }
    }
}
