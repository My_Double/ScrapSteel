﻿/************************************************************************
 * 项目名称 :  HttpSelfHost.Controllers  
 * 项目描述 :     
 * 类 名 称 :  CrownBridgeController
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/12 10:38:52
 * 更新时间 :  2020/3/12 10:38:52
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System;
using System.Collections.Generic;
using System.Web.Http;
using SG.RD.STS.Models;
using SG.RD.STS.Repository.Entity;

namespace HttpSelfHost.Controllers
{
    public class CrownBridgeController:ApiController
    {

        /// <summary>
        /// 获取全部地磅信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<CrownBridge> CrownBridgeList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取地磅分页信息
        /// </summary>
        /// <param name="info">分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public PageTable<CrownBridge> CrownBridgePage(PageInfo info)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 获取一条地磅信息
        /// </summary>
        /// <param name="crownBridge"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult GetCrownBridge(CrownBridge crownBridge)
        {
            throw new NotImplementedException();
        }
       
        /// <summary>
        /// 地磅接管冶炼计划
        /// </summary>
        /// <param name="crownBridge"></param>
        /// <param name="schedule"></param>
        /// <param name="bucket"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult ReadyTake(CrownBridge crownBridge, Schedule schedule,Bucket bucket)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 修改地磅信息
        /// </summary>
        /// <param name="crownBridge"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult UpdateCrownBridge(CrownBridge crownBridge)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 新增地磅信息
        /// </summary>
        /// <param name="crownBridge"></param>
        /// <returns></returns>
        [HttpPut]
        public IHttpActionResult AddCrownBridge(CrownBridge crownBridge)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 删除地磅信息
        /// </summary>
        /// <param name="crownBridge"></param>
        /// <returns></returns>
        [HttpDelete]
        public IHttpActionResult DeleteCrownBridge(CrownBridge crownBridge)
        {
            throw new NotImplementedException();
        }
    }
}
