﻿namespace SG.RD.STS.Interfaces
{
    public interface IPLC
    {
        /// <summary>
        /// 读取PLC数据
        /// </summary>
        /// <returns></returns>
        object ReadDB();
        /// <summary>
        /// 写入数据库
        /// </summary>
        /// <returns></returns>
        bool WriteDB();
        /// <summary>
        /// 写入PLC初始值
        /// </summary>
        void WritePLC();
        /// <summary>
        /// 重连PLC
        /// </summary>
        void Reconnect();
        /// <summary>
        /// 检查数据
        /// </summary>
        /// <param name="monitor"></param>
        /// <returns></returns>
        bool DoCheckData(object monitor);
    }

    public interface IPLCService
    {
        object UiMonitor { get;  set; }
        void StartRead();
        bool StartWrite();
        
    }
}