﻿/************************************************************************
 * 项目名称 :  HttpSelfHost.Controllers  
 * 项目描述 :     
 * 类 名 称 :  ScheduleController
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 19:54:03
 * 更新时间 :  2020/3/11 19:54:03
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System;
using System.Collections.Generic;
using System.Web.Http;
using SG.RD.STS.Models;
using SG.RD.STS.Repository.Entity;

namespace HttpSelfHost.Controllers
{
    public class ScheduleController:ApiController
    {
        /// <summary>
        /// 获取全部计划信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<Schedule> ScheduleList()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 获取单个计划
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]        
        public Schedule GetSchedule(int id)
        {            
            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取计划分页信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        [HttpPost]
        public PageTable<Schedule> SchedulePage(PageInfo info)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 修改计划信息
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult UpdateSchedule(Schedule schedule)
        {
            throw new NotImplementedException();            
        }
        /// <summary>
        /// 新增计划
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        [HttpPut]
        public IHttpActionResult AddSchedule(Schedule schedule)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 删除一个计划
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        [HttpDelete]
        public IHttpActionResult DeleteSchedule(Schedule schedule)
        {
            throw new NotImplementedException();
        }
    }
}
