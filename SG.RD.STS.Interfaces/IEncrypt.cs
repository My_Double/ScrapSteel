﻿namespace SG.RD.STS.Interfaces
{
    /// <summary>
    /// 数据加密操作类
    /// </summary>
    public interface IEncrypt
    {
        string md5(string password, int codeLength);
    }
}
