﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository  
 * 项目描述 :     
 * 类 名 称 :  DbFactory
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/2/26 9:40:13
 * 更新时间 :  2020/2/26 9:40:13
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SG.RD.STS.Repository
{
    public class DbFactory
    {
        public static IRepository Create()
        {
            return new EfRepository(new MyDBContext());
        }
    }
}
