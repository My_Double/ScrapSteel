﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Domain.Interfaces  
 * 项目描述 :     
 * 类 名 称 :  IConfig接口
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  接口
 * 作    者 :  张臣
 * 创建时间 :  2020/5/11 16:33:49
 * 更新时间 :  2020/5/11 16:33:49
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using SG.RD.STS.Models;

namespace SG.RD.STS.Interfaces
{
    public interface IConfig
    {
        Config ReadConfig();

        void SetConfig(Config config);
    }
}
