﻿namespace SG.RD.STS.Interfaces
{
    /// <summary>
    /// 被观察者接口
    /// </summary>
    public interface IObservable
    {
        void AddObserver(IObserver observer);
        void DeleteObserver(IObserver observer);
        void NotifyObserver(object context);
    }

    /// <summary>
    /// 观察者
    /// </summary>
    public interface IObserver
    {
        void Update(object context);
    }
}
