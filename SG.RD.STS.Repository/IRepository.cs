﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository  
 * 项目描述 :     
 * 类 名 称 :  IRepository
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/2/26 9:39:34
 * 更新时间 :  2020/2/26 9:39:34
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SG.RD.STS.Repository
{
    public interface IRepository:IDisposable
    {
        int SaveChanges();
        IQueryable<T> All<T>() where T : class;
        T Get<T>(Expression<Func<T, bool>> conditions) where T : class;
        T Insert<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        void Delete<T>(Expression<Func<T, bool>> conditions) where T : class;
        List<T> Find<T>(Expression<Func<T, bool>> conditions = null) where T : class;
        List<T> Find<T, S>(Expression<Func<T, bool>> conditions, Expression<Func<T, S>> orderBy, int pageSize, int pageIndex, out int totalCount) where T : class;
        List<T> SqlQuery<T>(string sql);
        int ExecuteSqlCommand(string sql);
        long GetNextSequenceValue(string sequenceName);
    }
}
