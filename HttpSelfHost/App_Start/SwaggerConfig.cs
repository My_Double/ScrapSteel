﻿using System;
using System.Web.Http;
using WebActivatorEx;
using Swashbuckle.Application;
using HttpSelfHost;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace HttpSelfHost
{
    public class SwaggerConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            config.EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "Scraptreatment");
                        c.IncludeXmlComments(GetXmlCommentsPath());
                        //c.IncludeXmlComments(GetXmlCommentsPath1());

                        //c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                    })
                .EnableSwaggerUi();
        }

        private static string GetXmlCommentsPath()
        {
            var basepath = AppDomain.CurrentDomain.BaseDirectory;
            var fileName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            return string.Format(@"{0}/Docs/{1}.xml", basepath, fileName);
        }
        private static string GetXmlCommentsPath1()
        {
            var basepath = AppDomain.CurrentDomain.BaseDirectory;
            var fileName = "SG.RD.STS.Repository";
            return string.Format(@"{0}/Docs/{1}.xml", basepath, fileName);
        }
    }
}

