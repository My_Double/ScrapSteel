﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Utility  
 * 项目描述 :     
 * 类 名 称 :  EumHelper
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/5/11 14:57:25
 * 更新时间 :  2020/5/11 14:57:25
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/
using System;
using System.ComponentModel.DataAnnotations;

namespace SG.RD.STS.Utility
{
    /// <summary>
    /// 获取枚举类的注释
    /// </summary>
    public static class EumHelper
    {        
        /// <summary>
        /// 获得枚举的displayName
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static string GetDisplayName(this Enum t)
        {
            var tType = t.GetType();
            var fieldName = Enum.GetName(tType, t);
            var objs = tType.GetField(fieldName).GetCustomAttributes(typeof(DisplayAttribute), false);
            return objs.Length > 0 ? ((DisplayAttribute)objs[0]).Name : null;
        }
    }
}
