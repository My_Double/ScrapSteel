﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace SG.RD.STS.Utility
{
    /// <summary>
    /// 读写Json配置文件
    /// </summary>
    public class SystemConfig
    {
        private const string path = "./Conf/MyConfig.json";
        private static JObject jObject = null;
        //private static SystemConfig jch;
        private static readonly object locker = new object();

        public string this[string key]
        {
            get
            {
                string str = "";
                if (jObject != null)
                {
                    str = GetValue(key);
                }
                return str;
            }
        }

        private SystemConfig()
        {
        }

        static SystemConfig()
        {
            jObject = new JObject();
            ReadJsonFile();
        }

        //static SystemConfig GetInstance()
        //{
        //    if (jch == null)
        //    {
        //        lock (locker)
        //        {
        //            if (jch == null)
        //            {
        //                jch = new SystemConfig();
        //            }
        //        }
        //    }

        //    return jch;
        //}
        static void ReadJsonFile()
        {
            if (!File.Exists(path))
            {
                Log.Error("配置文件不存在：" + path);
                return;
            }
            else
            {
                using (System.IO.StreamReader file = System.IO.File.OpenText(path))
                {
                    using (JsonTextReader reader = new JsonTextReader(file))
                    {
                        jObject = JObject.Load(reader);
                    }
                };
            }
        }

        /// <summary>
        /// 根据键值获取配置
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T GetValue<T>(string key) where T : class
        {
            return JsonConvert.DeserializeObject<T>(jObject.SelectToken(key).ToString());
        }


        /// <summary>
        /// 获取全部信息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetAllValue<T>() where T : class
        {
            return JsonConvert.DeserializeObject<T>(jObject.ToString());
        }

        public static string GetValue(string key)
        {
            return Regex.Replace((jObject.SelectToken(key).ToString()), @"\s", "");
        }

        /// <summary>
        /// 写入JSON
        /// </summary>
        /// <returns></returns>
        public static bool Write(object o)
        {
            try
            {
                if (o == null)
                {
                    return false;
                }
                string tmpJsonStr = JsonConvert.SerializeObject(o);

                File.WriteAllText(path, tmpJsonStr, Encoding.UTF8);

                //写入后更新当前使用的配置信息
                ReadJsonFile();

                return true;
            }
            catch (System.Exception ex)
            {
                Log.Error("保存结果异常" + ex.Message + ex.StackTrace);
                return false;
            }
        }

    }
}
