﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository.Entity  
 * 项目描述 :     
 * 类 名 称 :  Enums
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/2/26 9:42:40
 * 更新时间 :  2020/2/26 9:42:40
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System;
using System.ComponentModel.DataAnnotations;

namespace SG.RD.STS.Repository.Enums
{

    /// <summary>
    /// 用户权限
    /// </summary>
    public enum EPerm
    {
        Tmp = -1,
        Normal = 100,

        Perm6 = 600,

        /// <summary>
        /// 一级操作工
        /// </summary>
        [Display(Name = "一级操作工")]
        L1 = 700,
        /// <summary>
        /// 二级操作工
        /// </summary>
        [Display(Name = "二级操作工")]
        L2 = 800,
        /// <summary>
        /// 管理员
        /// </summary>
        [Display(Name = "管理员")]
        Admin = 900
    }

    /// <summary>
    /// 天车状态
    /// </summary>
    public enum CrownBlockStat
    {
        /// <summary>
        /// 停用
        /// </summary>
        [Display(Name = "停用")]
        OnStop = 0,
        /// <summary>
        /// 启用
        /// </summary>
        [Display(Name = "启用")]
        OnUse = 1,

        /// <summary>
        /// 检修
        /// </summary>
        [Display(Name = "检修")]
        OverHaul = 2
    }
    /// <summary>
    /// 磁力吸盘
    /// </summary>
    public enum SuckerStat
    {
        /// <summary>
        /// 去磁
        /// </summary>
        [Display(Name = "去磁")]
        Off = 0,
        /// <summary>
        /// 上磁
        /// </summary>
        [Display(Name = "上磁")]
        On = 1
    }
    /// <summary>
    /// 地磅状态
    /// </summary>
    public enum WeighBridgeStat
    {
        /// <summary>
        /// 停用
        /// </summary>
        [Display(Name = "停用")]
        Off = 0,
        /// <summary>
        /// 启用
        /// </summary>
        [Display(Name = "启用")]
        On = 1,
        /// <summary>
        /// 故障
        /// </summary>
        [Display(Name = "故障")]
        Broken = 2
    }
    /// <summary>
    /// 钢斗状态
    /// </summary>
    public enum BucketStat
    {
        /// <summary>
        /// 停用
        /// </summary>
        [Display(Name = "停用")]
        Off = 0,
        /// <summary>
        /// 启用
        /// </summary>
        [Display(Name = "启用")]
        On = 1,
        /// <summary>
        /// 故障
        /// </summary>
        [Display(Name = "故障")]
        Broken = 2,
        /// <summary>
        /// 转场
        /// </summary>
        [Display(Name = "转场")]
        Transition = 3
    }
    /// <summary>
    /// 计划状态
    /// </summary>
    public enum ScheduleStat
    {
        /// <summary>
        /// 计划可用
        /// </summary>
        [Display(Name = "计划可用")]
        Usalbe = 1,
        /// <summary>
        /// 本地接管
        /// </summary>
        [Display(Name = "本地接管")]
        Local = 2,
        /// <summary>
        /// 装载完成
        /// </summary>
        [Display(Name = "装载完成")]
        Loaded = 3,
        /// <summary>
        /// 发给供应
        /// </summary>
        [Display(Name = "发给供应")]
        Sended2Supply = 4,
        /// <summary>
        /// 发给转炉
        /// </summary>
        [Display(Name = "发给转炉")]
        Sended2Stove = 5,
        /// <summary>
        /// 转炉占用
        /// </summary>
        [Display(Name = "转炉占用")]
        StoveUsing = 6
    }

    /// <summary>
    /// 报警状态枚举
    /// </summary>
    public enum AlarmStatus
    {
        [Display(Name = "报警关闭")]
        OffAlarm = 0,
        [Display(Name = "正在报警")]
        OnAlarm = 1
    } 
}
