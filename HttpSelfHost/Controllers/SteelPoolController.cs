﻿
/************************************************************************
 * 项目名称 :  HttpSelfHost.Controllers  
 * 项目描述 :     
 * 类 名 称 :  SteelPoolController
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 16:41:33
 * 更新时间 :  2020/3/11 16:41:33
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System;
using System.Collections.Generic;
using System.Web.Http;
using SG.RD.STS.Models;
using SG.RD.STS.Repository.Entity;

namespace HttpSelfHost.Controllers
{
    public class SteelPoolController:ApiController
    {

        /// <summary>
        /// 获取全部废钢池信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<SteelPool> SteelPoolList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取废钢池分页信息
        /// </summary>
        /// <param name="info">分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public PageTable<SteelPool> SteelPoolPage(PageInfo info)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 获取一条废钢池信息
        /// </summary>
        /// <param name="steelPool"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult GetSteelPool(SteelPool steelPool)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 修改废钢池信息
        /// </summary>
        /// <param name="steelPool"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult UpdateSteelPool(SteelPool steelPool)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 新增废钢池信息
        /// </summary>
        /// <param name="steelPool"></param>
        /// <returns></returns>
        [HttpPut]
        public IHttpActionResult AddSteelPool(SteelPool steelPool)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 删除废钢池信息
        /// </summary>
        /// <param name="steelPool"></param>
        /// <returns></returns>
        public IHttpActionResult DeleteSteelPool(SteelPool steelPool)
        {
            throw new NotImplementedException();
        }
    }
}
