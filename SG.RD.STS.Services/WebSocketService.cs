﻿using System.Collections.Generic;
using System.Linq;
using Fleck;
using SG.RD.STS.Interfaces;
using SG.RD.STS.Utility;
using SG.RD.STS.Utility.Json;

namespace SG.RD.STS.Services
{
    public class WebSocketService : IWebSocketService
    {
        private static List<IWebSocketConnection> allSockets = new List<IWebSocketConnection>();

        public void Send(object msg)
        {
            var datastr = JsonConverter.ToJson(msg);

            foreach (var socket in allSockets.ToList())
            {
                socket.Send(datastr);
            }
        }

        public void Client()
        {
            
        }

        public void ServerStart(string ip)
        {
            var server = new WebSocketServer(string.Format("ws://{0}:7183", ip));
            server.Start(socket =>
            {
                socket.OnOpen = () =>
                {
                    //Console.WriteLine("Open!");
                    allSockets.Add(socket);
                };
                socket.OnClose = () =>
                {
                    //Console.WriteLine("Close!");
                    allSockets.Remove(socket);
                };
                socket.OnMessage = message =>
                {
                    //Console.WriteLine(message);
                    allSockets.ToList().ForEach(s => s.Send("Echo: " + message));
                };
            });
        }


    }
}