﻿/************************************************************************
 * 项目名称 :  SG.RD.STS.Repository.Entity  
 * 项目描述 :  废钢池   
 * 类 名 称 :  SteelPool
 * 版 本 号 :  v1.0.0.0 
 * 说    明 :  用于记录废钢池的数据库实体
 * 作    者 :  张臣
 * 创建时间 :  2020/3/11 10:21:02
 * 更新时间 :  2020/3/11 10:21:02
************************************************************************
 * Copyright  2020. All rights reserved.
************************************************************************/

using System.ComponentModel.DataAnnotations.Schema;

namespace SG.RD.STS.Repository.Entity
{
    /// <summary>
    /// 废钢池
    /// </summary>
    [Table("STEELPOOL")]
    public class SteelPool:BaseEntity
    {
        /// <summary>
        /// 废钢槽号
        /// </summary>
        [Column("SLOTNO")]
        public int SlotNo { get; set; }
        /// <summary>
        /// X最小值
        /// </summary>
        [Column("XMIN")]
        public int XMin { get; set; }
        /// <summary>
        /// X最大值
        /// </summary>
        [Column("XMAX")]
        public int XMax { get; set; }
        /// <summary>
        /// Y最小值
        /// </summary>
        [Column("YMIN")]
        public int YMin { get; set; }
        /// <summary>
        /// Y最大值
        /// </summary>
        [Column("YMAX")]
        public int YMax { get; set; }
        /// <summary>
        /// 物料Id
        /// </summary>
        [Column("STEELID")]
        public int SteelId { get; set; }
    }
}
