﻿namespace HttpSelfHost.Forms
{
    partial class SitConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SitConfig));
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.NotifyMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiHome = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiMgr = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiDir = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiRestart = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiQuit = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txbAddr = new System.Windows.Forms.TextBox();
            this.txbPort = new System.Windows.Forms.TextBox();
            this.txbDir = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnPort = new System.Windows.Forms.Button();
            this.btnDir = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnRestart = new System.Windows.Forms.Button();
            this.btnManager = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.chb = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.NotifyMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.NotifyMenu;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "废钢系统";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseClick);
            // 
            // NotifyMenu
            // 
            this.NotifyMenu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.NotifyMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiConfig,
            this.cmiHome,
            this.cmiMgr,
            this.cmiDir,
            this.cmiRestart,
            this.cmiQuit});
            this.NotifyMenu.Name = "contextMenuStrip1";
            this.NotifyMenu.Size = new System.Drawing.Size(161, 206);
            // 
            // cmiConfig
            // 
            this.cmiConfig.AutoSize = false;
            this.cmiConfig.Image = global::HttpSelfHost.Properties.Resources.globe1;
            this.cmiConfig.Name = "cmiConfig";
            this.cmiConfig.Size = new System.Drawing.Size(160, 30);
            this.cmiConfig.Text = "打开配置窗口";
            this.cmiConfig.Click += new System.EventHandler(this.cmiConfig_Click);
            // 
            // cmiHome
            // 
            this.cmiHome.AutoSize = false;
            this.cmiHome.Image = global::HttpSelfHost.Properties.Resources.home;
            this.cmiHome.Name = "cmiHome";
            this.cmiHome.Size = new System.Drawing.Size(160, 30);
            this.cmiHome.Text = "打开站点首页";
            this.cmiHome.Click += new System.EventHandler(this.cmiHome_Click);
            // 
            // cmiMgr
            // 
            this.cmiMgr.AutoSize = false;
            this.cmiMgr.Image = global::HttpSelfHost.Properties.Resources.laptop;
            this.cmiMgr.Name = "cmiMgr";
            this.cmiMgr.Size = new System.Drawing.Size(160, 30);
            this.cmiMgr.Text = "打开站点后台";
            this.cmiMgr.Click += new System.EventHandler(this.cmiMgr_Click);
            // 
            // cmiDir
            // 
            this.cmiDir.AutoSize = false;
            this.cmiDir.Image = global::HttpSelfHost.Properties.Resources.folder;
            this.cmiDir.Name = "cmiDir";
            this.cmiDir.Size = new System.Drawing.Size(160, 30);
            this.cmiDir.Text = "打开站点目录";
            this.cmiDir.Click += new System.EventHandler(this.cmiDir_Click);
            // 
            // cmiRestart
            // 
            this.cmiRestart.AutoSize = false;
            this.cmiRestart.Image = global::HttpSelfHost.Properties.Resources.refresh;
            this.cmiRestart.Name = "cmiRestart";
            this.cmiRestart.Size = new System.Drawing.Size(160, 30);
            this.cmiRestart.Text = "重启站点服务";
            this.cmiRestart.Click += new System.EventHandler(this.cmiRestart_Click);
            // 
            // cmiQuit
            // 
            this.cmiQuit.AutoSize = false;
            this.cmiQuit.Image = global::HttpSelfHost.Properties.Resources.stop;
            this.cmiQuit.Name = "cmiQuit";
            this.cmiQuit.Size = new System.Drawing.Size(160, 30);
            this.cmiQuit.Text = "退出站点";
            this.cmiQuit.Click += new System.EventHandler(this.cmiQuit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(24, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "地址：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(24, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 14);
            this.label2.TabIndex = 1;
            this.label2.Text = "端口：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(24, 191);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 1;
            this.label3.Text = "站点路径：";
            // 
            // txbAddr
            // 
            this.txbAddr.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txbAddr.Location = new System.Drawing.Point(116, 123);
            this.txbAddr.Name = "txbAddr";
            this.txbAddr.Size = new System.Drawing.Size(311, 22);
            this.txbAddr.TabIndex = 2;
            // 
            // txbPort
            // 
            this.txbPort.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txbPort.Location = new System.Drawing.Point(116, 155);
            this.txbPort.Name = "txbPort";
            this.txbPort.Size = new System.Drawing.Size(311, 22);
            this.txbPort.TabIndex = 2;
            // 
            // txbDir
            // 
            this.txbDir.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txbDir.Location = new System.Drawing.Point(116, 187);
            this.txbDir.Name = "txbDir";
            this.txbDir.Size = new System.Drawing.Size(311, 22);
            this.txbDir.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(71, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 14);
            this.label4.TabIndex = 1;
            this.label4.Text = "http://";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnAdd.Location = new System.Drawing.Point(447, 123);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(91, 23);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "更改地址";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnPort
            // 
            this.btnPort.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnPort.Location = new System.Drawing.Point(447, 155);
            this.btnPort.Name = "btnPort";
            this.btnPort.Size = new System.Drawing.Size(91, 23);
            this.btnPort.TabIndex = 3;
            this.btnPort.Text = "更改端口";
            this.btnPort.UseVisualStyleBackColor = true;
            this.btnPort.Click += new System.EventHandler(this.btnPort_Click);
            // 
            // btnDir
            // 
            this.btnDir.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnDir.Location = new System.Drawing.Point(447, 187);
            this.btnDir.Name = "btnDir";
            this.btnDir.Size = new System.Drawing.Size(91, 22);
            this.btnDir.TabIndex = 3;
            this.btnDir.Text = "打开目录";
            this.btnDir.UseVisualStyleBackColor = true;
            this.btnDir.Click += new System.EventHandler(this.btnDir_Click);
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnStop.Location = new System.Drawing.Point(446, 218);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(91, 23);
            this.btnStop.TabIndex = 3;
            this.btnStop.Text = "停止服务";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnRestart
            // 
            this.btnRestart.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnRestart.Location = new System.Drawing.Point(336, 218);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(91, 23);
            this.btnRestart.TabIndex = 3;
            this.btnRestart.Text = "重启服务";
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // btnManager
            // 
            this.btnManager.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnManager.Location = new System.Drawing.Point(226, 218);
            this.btnManager.Name = "btnManager";
            this.btnManager.Size = new System.Drawing.Size(91, 23);
            this.btnManager.TabIndex = 3;
            this.btnManager.Text = "打开后台";
            this.btnManager.UseVisualStyleBackColor = true;
            this.btnManager.Click += new System.EventHandler(this.btnManager_Click);
            // 
            // btnHome
            // 
            this.btnHome.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnHome.Location = new System.Drawing.Point(116, 218);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(91, 23);
            this.btnHome.TabIndex = 3;
            this.btnHome.Text = "打开网站";
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // chb
            // 
            this.chb.AutoSize = true;
            this.chb.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chb.Location = new System.Drawing.Point(27, 221);
            this.chb.Name = "chb";
            this.chb.Size = new System.Drawing.Size(82, 18);
            this.chb.TabIndex = 4;
            this.chb.Text = "开机运行";
            this.chb.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(604, 97);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // SitConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 255);
            this.Controls.Add(this.chb);
            this.Controls.Add(this.btnHome);
            this.Controls.Add(this.btnManager);
            this.Controls.Add(this.btnRestart);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnDir);
            this.Controls.Add(this.btnPort);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txbDir);
            this.Controls.Add(this.txbPort);
            this.Controls.Add(this.txbAddr);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SitConfig";
            this.Text = "废钢系统网站服务器";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SitConfig_FormClosing);
            this.Load += new System.EventHandler(this.SitConfig_Load);
            this.SizeChanged += new System.EventHandler(this.SitConfig_SizeChanged);
            this.NotifyMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip NotifyMenu;
        private System.Windows.Forms.ToolStripMenuItem cmiConfig;
        private System.Windows.Forms.ToolStripMenuItem cmiHome;
        private System.Windows.Forms.ToolStripMenuItem cmiMgr;
        private System.Windows.Forms.ToolStripMenuItem cmiRestart;
        private System.Windows.Forms.ToolStripMenuItem cmiQuit;
        private System.Windows.Forms.ToolStripMenuItem cmiDir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txbAddr;
        private System.Windows.Forms.TextBox txbPort;
        private System.Windows.Forms.TextBox txbDir;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnPort;
        private System.Windows.Forms.Button btnDir;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.Button btnManager;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.CheckBox chb;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}