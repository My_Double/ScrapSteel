﻿using SG.RD.STS.Utility;

namespace SG.RD.STS.Models.PLC
{
    /// <summary>
    /// Plc缓冲区类
    /// </summary> 
    public class PLCBuffer
    {
        public int ReadLowStart { get; set; }
        public int ReadHighStart { get; set; }
        public int WriteStart { get; set; }
        public int ReadLowSize { get; set; }
        public int ReadHighSize { get; set; }
        public int WriteSize { get; set; }
        public int ReadNo { get; set; }
        public int WriteNo { get; set; }
        public int ReadAllStart { get; set; }
        public int ReadAllSize { get; set; }

        private PLCBuffer()
        {
        }

        public static PLCBuffer Instance
        {
            get { return SystemConfig.GetValue<PLCBuffer>("Buffer"); }
        }

    }
}